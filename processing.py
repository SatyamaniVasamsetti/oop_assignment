from abc import ABC, abstractmethod
from orders import Orders
from Customer import Customer
from Shipper import Shipper
from products import products,ElectronicItem,BeautyItems


class OrderProcessor(ABC):
    @abstractmethod
    def process_order(self, order: Orders):
        pass


class ShippingProcessor(OrderProcessor):
    def process_order(self, order: Orders):
        # Code to ship the products to the customer's address
        print(f"Order for {order.customer.name} shipped by {order.shipper.name}")


# Example usage
customer = Customer("101", "John", "Gachibowli","834984747858","xyz@gmail.com")
shipper = Shipper("ssr infra","abc@gmail.com","Hyderabad")

product1 = products(1,"Hp Laptop",1,40588,4.5,ElectronicItem(4))
product2 = products(2,"Dell Laptop",1,41388,4.5,ElectronicItem(4))
# print(product1.total_price())
order = Orders("1012","12-09-2022",customer,[product1,product2],shipper)
print(order.get_total_price())  # 19.98

s1 = ShippingProcessor()
print(s1.process_order(order))
