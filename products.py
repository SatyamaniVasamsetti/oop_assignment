class products:
    def __init__(self,product_id,product_name,quantity,unit_price,ratings,category):
        self.product_id=product_id
        self.product_name=product_name
        self.ratings=ratings
        self.unit_price=unit_price
        self.quantity=quantity
        self.category=category


    def total_price(self):
        return (self.unit_price) * self.quantity


class ElectronicItem(products):

    def __init__(self,warranty_in_months):
        self.warranty_in_months=warranty_in_months


    def display_product_details(self):
        print("Product: {}".format(self.product_name))
        print("Price: {}".format(self.unit_price))
        print("Rating: {}".format(self.ratings))
        print("Warraty {} months".format(self.warranty_in_months))

    def get_warranty(self):
        return self.warranty_in_months



class BeautyItems(products):
    def __init__(self,product_id,product_name,quantity,unit_price,ratings,expiry_date,manufacture_date):
        super().__init__(product_id,product_name,quantity,unit_price,ratings)
        self.expiry_date=expiry_date
        self.manufacture_date=manufacture_date

    def display_product_details(self):
        print("Product: {}".format(self.product_name))
        print("Price: {}".format(self.unit_price))
        print("Rating: {}".format(self.ratings))
        print("Expiry Date: {}".format(self.expiry_date))
        print("Manufacture Date: {}".format(self.manufacture_date))


# e1=ElectronicItem(1,"Hp Laptop",1,"40588",4.5,4)
#
# print(e1.total_price())
# print(e1.display_product_details())
#



product = products(1,"Hp Laptop",1,"40588",4.5,ElectronicItem(4))
print(product.total_price())