from Customer import Customer
from Shipper import Shipper
from products import products
from typing import List

class Orders:
    def __init__(self,order_id, customer:Customer, orderDate, products: List[products], shipper: Shipper):
        self.order_id=order_id
        self.orderDate=orderDate
        self.customer = customer
        self.products = products
        self.shipper = shipper

    def get_total_price(self) -> float:
        total_price = sum([product.unit_price for product in self.products])
        return total_price





